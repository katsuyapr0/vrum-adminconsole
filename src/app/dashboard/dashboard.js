(function(module) {

	module.controller('DashboardController', ['DashboardModelService', 'CampaignDialog', function(DashboardModelService, CampaignDialog) {
		var model = this;
		// Vendor del dashboard
		model.vendor = {};
		// Campañas del Vendor
		model.campaigns = [];
		// Funcion para añadir campaña
		model.addCampaign = addCampaign;

		init();

		function addCampaign() {
			CampaignDialog.show();
		}

		function init() {
			// Llama el servicio de inicialización del modelo del dashboard y asigna valores luego de hacer todo su proceso
			DashboardModelService.init(function(vendor) {
				console.log(vendor);
				model.vendor = vendor;
				model.campaigns = vendor.campaigns;
			});
		}
	}]);

}(angular.module("vrumProject.dashboard")));
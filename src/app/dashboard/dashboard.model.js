(function(module) {

  function DashboardModelService($http, DashboardWebServices, Endpoint, $state) {
    var _self = this;

    // Funcion expuesta
    _self.init = init;

    // Desarrollo de funcion
    function init(save) {
      // Funcion que se activa cuando obtiene correctamente la informacion del vendedor
      function success(data) {

        var user = data.response;

        // Funcion que se activa cuando obtiene correctamente las campañas activas del Vendor
        function successActiveCampaigns(data) {
          console.log(data);
          user.campaigns = data.response;
          save(user);
        }

        // Funcion que se activa cuando no puede obtener las campañas activas del vendor
        function errorActiveCampaigns(data) {
          console.log(data);
          save(user);
        }

        // Llama al servicio para obtener las campañas activas del Vendor
        DashboardWebServices.getVendorActiveCampaings().success(successActiveCampaigns).error(errorActiveCampaigns);
      }

      // Funcion que se activa cuando no pudo obtener la información del Vendor
      function error(data) {
        console.log(data);
        // TODO: Show a message to user
      }

      // Llama al servicio para obtener la informacion del Vendor
      DashboardWebServices.getVendorInfo().success(success).error(error);
    }
  }

  module.service('DashboardModelService', [
    '$http',
    'DashboardWebServices',
    'Endpoint',
    '$state',
    DashboardModelService
  ]);

}(angular.module("vrumProject.dashboard")));
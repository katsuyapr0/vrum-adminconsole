(function(module) {

  module.service('DashboardWebServices', ['$http', 'Endpoint', function($http, Endpoint) {
    var _self = this;

    // Metodos expuestos
    _self.getVendorInfo = getVendorInfo;
    _self.getVendorActiveCampaings = getVendorActiveCampaings;

    // Desarrollo de funciones 

    // Funcion para obtener la informacion del vendedor
    function getVendorInfo() {
      return $http({
        url: Endpoint.getVendor() + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        //headers: Endpoint.getExpiredHeaders(), // Only for testing expired session
        method: 'GET',
      });
    }

    // Funcion para obtener las campañas activas del Vendor
    function getVendorActiveCampaings() {
      console.log('endpoint', Endpoint.getVendor() + 'Campaigns/Active/' + Endpoint.getSessionId() + '/');
      return $http({
        url: Endpoint.getVendor() + 'Campaigns/Active/' + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'GET',
      });
    }
  }]);

})(angular.module("vrumProject.dashboard"));
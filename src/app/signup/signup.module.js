(function(module) {

  module.config(function($stateProvider) {
    // Configura la ruta del vendor
    $stateProvider.state('signup', {
      url: '/signup',
      views: {
        "main": {
          controller: 'SignupController as model',
          templateUrl: 'signup/signup.tpl.html'
        }
      },
      data: {
        pageTitle: 'Signup'
      }
    });
  });

}(angular.module("vrumProject.signup", [
  'ui.router'
])));
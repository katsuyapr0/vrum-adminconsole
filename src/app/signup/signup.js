(function(module) {

	module.controller('SignupController', ['$scope', 'SignupModelService', '$state', function($scope, SignupModelService, $state) {
		var USER_MESSAGES = {
				// Nombre de los campos de la seccion 'bussiness' para ser mostrados al usuario
				bussiness: {
					name: 'Name',
					address1: 'Address 1',
					address2: 'Address 2',
					city: 'City',
					state: 'State',
					zipCode: 'Zip Code',
					phone: 'Phone',
					hoursOperation: 'Hours of operation',
				},
				// Nombre de los campos de la seccion 'billing' para ser mostrados al usuario
				billing: {
					number: 'Number of the card',
					security: 'Security code of the card',
					expirate: 'Expiration date',
					address: 'Billing address',
					firstName: 'First Name',
					LastName: 'Last Name',
					month: 'Expirate Month',
					year: 'Expirate Year'
				},
				// Nombre de los campos de la seccion 'contact' para ser mostrados al usuario
				contact: {
					name: 'Name',
					lastName: 'Last name',
					phone: 'Phone',
					username: 'Username',
					email: 'Email',
					password: 'Password',
					confirmationPassword: 'Confirmation of password',
				},
				// Nombre de los campos de la seccion 'mph' para ser mostrados al usuario
				mph: {
					name: 'Name',
					slogan: 'Slogan',
					description: 'description',
				}
			},
			// Numero de pasos de el registro
			STEPS = 4;

		var model = this,
			stepSelected = 0,
			isSignup = false;

		$scope.imageSrc = '';

		_init();

		// Metodos expuestos

		model.isSelected = isSelected;
		model.next = next;
		model.back = back;
		model.skip = skip;
		model.setSelected = setSelected;
		model.isDisabled = isDisabled;
		model.onClickStep = onClickStep;
		model.updateInfo = updateInfo;

		// Desarrollo de metodos

		// Verifica si el usuario ya creo cuenta y de acuerdo a esto envia a la funcion adecuada para verificar
		function validate(step) {
			if (!isSignup) {
				return preSignupValidate(step);
			}
			return postSignupValidate(step);
		}

		// FUncion de binding que verifica si un 'paso' esta desactivado
		function isDisabled(step) {
			var result = validate(step - 1);
			if (isSignup) {
				return true;
			}

			if (!result) {
				return false;
			}
			return step > stepSelected;
		}

		// Funcion de binding para verificar si se esa en un 'paso'
		function isSelected(step) {
			return step === stepSelected;
		}

		// Funcion que genera el objeto para enviar al servicio web
		function objectToSend() {
			return {
				email: model.contact.email,
				password: model.contact.password,
				name: model.bussiness.name,
				main_address: {
					city: model.bussiness.city,
					state: model.bussiness.state,
					zip: model.bussiness.zipCode,
					street1: model.bussiness.address1,
					street2: model.bussiness.address2,
					operation_time: {
						text: model.bussiness.hoursOperation,
					},
				},
				contact_info: {
					name: model.contact.name,
					lastname: model.contact.lastName,
					phone: model.contact.phone,
				},
				credit_card: {
					firstname: model.billing.firstName,
					lastname: model.billing.lastName,
					number: model.billing.card.number,
					ccv: model.billing.card.security,
					expire_month: 12,
					expire_year: 21,
				}
			};
		}

		// Funcion para el boton skip
		function skip() {
			$state.go('dashboard');
		}

		// Funcion para actualizar informacion del mph
		function updateInfo() {
			// valida campos del paso 4
			var result = _validateStep4();

			// result es un array con los campos que faltan si existe es porque faltan campos
			if (result) {
				// Manda un mensaje con los campos faltantes o con errores
				alert('You are missing this fields: ' +
					result.map(function(item) {
						return USER_MESSAGES.mph[item];
					}).join(', '));
			} else {
				// Si no hay error llama al servicio para que actualize el mph
				SignupModelService.onUpdateInfo(model.myFile, model.mph,
					function success(data) {
						console.log(data);
						$state.go('dashboard');
					},
					function error(data) {
						console.log(data);
						alert('Oops! something wrong happens, please try later!');
					});
			}
		}

		// Función cuando el usuario avanza de paso
		function next() {
			// llama a la funcion que valida
			var result = validate(stepSelected);
			console.log(result);
			// SI hay errores en el paso
			if (result) {
				var constant = {};
				// Dependiendo del paso se asigna las constantes de errores para mostar al usuario
				switch (stepSelected) {
					case 0:
						constant = USER_MESSAGES.bussiness;
						break;
					case 1:
						constant = USER_MESSAGES.contact;
						break;
						/* case 2:
							constant = USER_MESSAGES.billing;
							break; */
					case 2:
						// En el ultimo paso no se necesita llenar ningun formulario entonces solo se llama al servicio
						SignupModelService.onSignup(objectToSend(), function(result) {
							if (result) {
								isSignup = true;
								stepSelected++;
							}
						});
						return;
					case 3:
						constant = USER_MESSAGES.mph;
						return;
				}
				// Muestra al usuario los campos que le hacen falta
				alert('You are missing this fields: ' +
					result.map(function(item) {
						return constant[item];
					}).join(', '));
			} else {
				// Si no hay erroes se avanza
				if (stepSelected++ >= STEPS) {
					$state.go('dashboard');
				}
			}
		}

		// Funcion para devolverse de paso
		function back() {
			stepSelected--;
		}

		// funcion para seleccionar un numero de paso especifico
		function setSelected(step) {
			stepSelected = step;
		}

		// Funcion cuando se hace click sobre un tab
		function onClickStep(step) {
			if (isDisabled(step)) {
				return;
			}

			setSelected(step);
		}

		// Funcion para validar paso 1
		function _validateStep1() {
			var errors = [];
			for (var key in model.bussiness) {
				if (model.bussiness[key] === '') {
					errors.push(key);
				}
			}

			if (errors.length !== 0) {
				return errors;
			}

			return false;
		}

		// Funcion para validar paso 2
		function _validateStep2() {
			var errors = [];
			for (var key in model.contact) {
				if (model.contact[key] === '') {
					errors.push(key);
				}
			}

			if (errors.length !== 0) {
				return errors;
			}

			return false;
		}

		// Funcion para validar paso 3
		function _validateStep3() {
			var errors = [];
			for (var key in model.billing) {
				if (model.billing[key] === '') {
					errors.push(key);
				} else if (key === 'card') {
					for (var cardKey in model.billing[key]) {
						if (model.billing[key][cardKey] === '') {
							errors.push(cardKey);
						}
					}
				}
			}

			if (errors.length !== 0) {
				return errors;
			}

			return false;
		}

		// Funcion para validar paso 4
		function _validateStep4() {
			var errors = [];
			for (var key in model.mph) {
				if (model.mph[key] === '') {
					errors.push(key);
				}
			}

			if (errors.length !== 0) {
				return errors;
			}

			return false;
		}

		// Si ya creo cuenta los pasos 1,2,3 ya no pueden ser activados por el usuario
		function postSignupValidate(step) {
			switch (step) {
				case 0:
				case 1:
				case 2:
					return false;
			}
			return true;
		}

		// Valida si esta activo un campo para ser modificado antes de que se cree el usuario
		function preSignupValidate(step) {
			switch (step) {
				case 0:
					return _validateStep1();
				case 1:
					return _validateStep2();
				case 2:
					return true;
					//return _validateStep3();
				case 3:
					return _validateStep4();
			}
			return true;
		}

		function _init() {
			model.bussiness = {
				name: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zipCode: '',
				phone: '',
				hoursOperation: '',
			};
			model.billing = {
				card: {
					number: '',
					security: '',
					expirate: {
						month: '',
						year: '',
					},
				},
				address: '',
				firstName: '',
				lastName: ''
			};
			model.contact = {
				name: '',
				lastName: '',
				phone: '',
				email: '',
				password: '',
				confirmationPassword: '',
			};
			model.mph = {
				name: '',
				slogan: '',
				description: '',
			};
		}
	}]);

}(angular.module("vrumProject.signup")));
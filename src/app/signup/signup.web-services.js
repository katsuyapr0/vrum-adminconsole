(function(module) {

	module.service('SignupWebServices', ['$http', 'Endpoint', 'fileUpload', 'VendorInfo', function($http, Endpoint, fileUpload, VendorInfo) {
		var _self = this;

		// Metodos expuestos
		_self.createVendor = createVendor;
		_self.sendMph = sendMph;
		_self.updateMhpInfo = updateMhpInfo;

		// Desarrollo de las funciones
		// Llama al servicio para subir una imagen a mph
		function sendMph(file, success, error) {
			var url = Endpoint.getVendor() + 'AddFile/' + Endpoint.getSessionId() + '/',
				type = 'mhp';

			fileUpload.uploadFileToUrl(file, url, type, success, error);
		}

		// Llama al servicio para actualizar la informacion de mph
		function updateMhpInfo(mhp) {
			VendorInfo.vendor.mobile_homepage =  {
				name: mhp.name || VendorInfo.vendor.mobile_homepage.name,
				slogan: mhp.slogan || VendorInfo.vendor.mobile_homepage.slogan,
				description: mhp.description || VendorInfo.vendor.mobile_homepage.description,
			};

			return $http({
				url: Endpoint.getVendor() + VendorInfo.vendor._id + '/',
				data: VendorInfo,
				method: 'PUT'
			});
		}

		// Llama la funcion para crear un vendor
		function createVendor(vendor) {
			return $http({
				url: Endpoint.getVendor(),
				data: vendor,
				method: 'POST',
			});
		}
	}]);

})(angular.module("vrumProject.signup"));
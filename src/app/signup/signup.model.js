(function(module) {
	var _self = this;

	module.service('SignupModelService', ['SignupWebServices', '$state', 'Endpoint', 'VendorInfo', function(SignupWebServices, $state, Endpoint, VendorInfo) {
		var _self = this;

		// Metodos expuestos
		_self.onSignup = onSignup;
		_self.onUpdateInfo = onUpdateInfo;

		// Funcion para actualizar informacion del Vendor, mas especificamente enviar informacion del mph
		function onUpdateInfo(file, info, success, error) {
			// sube el archivo del mph
			SignupWebServices.sendMph(file, function(data) {
				if(data.status) {
					// si se puede subir se actualiza la otra informacion
					SignupWebServices.updateMhpInfo(info).success(success, error);
				} else {
					error();
				}
			}, error);
		}

		// Funcion para el flow de signup
		function onSignup(vendor, result) {
			var password = vendor.password;
			// Encriptacion b64 de la contraseña
			vendor.password = btoa(vendor.password);

			// Si se creo el usuario se guarda el sessionId, el usuario, la contraseña y la data del vendor
			function success(data) {
				console.log(data);
				if(result) {
					result(data.status);
				}
				if(data.status) {
          Endpoint.setSessionId(data.response._id);
          Endpoint.setUser(vendor.email, password);
          VendorInfo.vendor = data.response;
					//$state.go('dashboard');
				} else {
					alert(data.message);
				}
			}

			function error(data) {
				console.log(data);
				if(result) {
					result(false);
				}
				alert(data.error);
			}

			// Llama al servicio para crear un vendor
			SignupWebServices.createVendor(vendor).success(success).error(error);
		}
	}]);

})(angular.module("vrumProject.signup"));
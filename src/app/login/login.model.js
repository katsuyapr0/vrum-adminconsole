(function(module) {

  function LoginModelService($http, LoginWebServices, Endpoint, $state) {
    var _self = this;

    // Metodos expuestos
    _self.getVendor = getVendor;
    _self.onLogin = onLogin;

    // Desarrollo de funciones

    // Funcion para obtener información del vendor
    function getVendor() {
      // Cuando logra obtener la informacion del Vendor
      function success(data) {
        console.log(data);
      }

      // Cuando no logra obtener la informacion del Vendor
      function error(data) {
        console.log(data);
      }

      // Llama el servicio para obtener la información del vendor
      LoginWebServices.getVendorInfo().success(success).error(error);
    }

    // Funcion para llamar el servicio de autenticación
    function onLogin(email, password) {
      // Cuando la autenticación es correcta
      function success(data) {
        console.log(data);
        // Verifica el status del servicio
        if(data.status) {
          Endpoint.setSessionToken(data.token.token);
          Endpoint.setSessionId(data.response._id);
          Endpoint.setUser(email, password);

          $state.go('dashboard');
        } else {
          alert('Something It\'s wrong. Please check your email or password.');
        }
      }
      // Cuando la autenticación es incorrecta
      function error(data) {
        alert('Something It\'s wrong. Please check your internet conection.');
      }

      // Llama el servicio para autenticarse
      LoginWebServices.loginVendor(email, password).success(success).error(error);
    }
  }

  module.service('LoginModelService', [
    '$http',
    'LoginWebServices',
    'Endpoint',
    '$state',
    LoginModelService
  ]);

}(angular.module("vrumProject.login")));
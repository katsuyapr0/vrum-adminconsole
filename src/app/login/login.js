(function(module) {

  function LoginController($http, LoginWebServices, Endpoint, LoginModelService) {
    var model = this,
      mail = '';

    _init();

    // Metodos expuestos
    model.onLogin = onLogin;

    // Funcion para validar emails
    function validateEmail(email) {
      var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
    }

    // funcion para verificar formulario de login
    function onLogin() {
      if (model.email === '') {
        alert('The email is empty, please enter that.');
        return;
      } else if(!validateEmail(model.email)) {
        alert('This email doesn\'t exist.');
        return;
      }

      if (model.password === '') {
        alert('The password is empty, please enter that.');
        return;
      }

      // Llama al modelo para que llame al servicio de login
      LoginModelService.onLogin(model.email, model.password);
    }

    function _init() {
      model.email = '';
      model.password = '';
    }
  }

  module.controller('LoginController', [
    '$http',
    'LoginWebServices',
    'Endpoint',
    'LoginModelService',
    LoginController
  ]);

}(angular.module("vrumProject.login")));
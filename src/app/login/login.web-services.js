(function(module) {

	module.service('LoginWebServices', ['$http', 'Endpoint', function($http, Endpoint) {
		var _self = this;

		// Metodos expuestos
		_self.loginVendor = loginVendor;
		_self.getVendorInfo = getVendorInfo;

		//Desarrollo de funciones
		// Funcion para llamar el servicio de autenticación
		function loginVendor(email, password) {
			return $http({
				url: Endpoint.getVendor() + 'Authenticate/',
				data: {
					email: email,
					password: btoa(password),
				},
				method: 'POST',
			});
		}
		// Funcion para obtener la informacion del Vendor
		function getVendorInfo() {
			return $http({
				url: Endpoint.getVendor()  + Endpoint.getSessionId() + '/',
				headers: Endpoint.getGlobalHeaders(),
				//headers: Endpoint.getExpiredHeaders(), // Only for testing expired session
				method: 'GET',
			});
		}
	}]);

})(angular.module("vrumProject.login"));
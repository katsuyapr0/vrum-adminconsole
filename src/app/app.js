(function(app) {
  app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home'); // Ruta predefinida si se ingresa una url no definida
  });

  app.run(function() {});

  //Controlador padre de toda la aplicación
  app.controller('AppController', function($scope) {

  });

  // Constante para definir el endpoint de los servicios web
  app.constant('EndpointConstant', {
    dev: 'http://192.241.187.135:2100/v1/',
    //dev: 'http://andres.local:2100/v1/',
    production: '',
    isDebug: true,
  });

  app.constant('VendorInfo', {
    vendor: {},
  });

  // valores que se van a usar en toda la aplicación
  app.value('SessionValue', {
    token: '',
    id: '',
    email: '',
    password: '',
  });

  // Servicio para obtener el endpoint
  app.service('Endpoint', ['EndpointConstant', 'SessionValue', function(EndPointConstant, SessionValue) {
    var _self = this,

      // rutas de los servicios(solo esta Vendor, pero si se crean más servicios hay que agregarlo aqui)
      OBJECTS = {
        vendor: 'Vendor',
      };

    // ----------------------------------------------------------------------------------------------------
    // Funciones expuestas en el Endpoint' service
    // ----------------------------------------------------------------------------------------------------
    _self.getGlobalHeaders = getGlobalHeaders;
    _self.getExpiredHeaders = getExpiredHeaders;
    _self.setSessionToken = setSessionToken;
    _self.setSessionId = setSessionId;
    _self.getVendor = getVendor;
    _self.setUser = setUser;
    _self.getSessionId = getSessionId;

    // ----------------------------------------------------------------------------------------------------
    // Desarrollo de las funciones
    // ----------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------------
    // Funciones privadas
    // ----------------------------------------------------------------------------------------------

    // Función para obtener el endpoint, dependiendo de si esta en debug o en production
    function _getEndPoint() {
      return EndPointConstant.isDebug ? EndPointConstant.dev : EndPointConstant.production;
    }

    // ----------------------------------------------------------------------------------------------
    // Funciones publicas
    // ----------------------------------------------------------------------------------------------

    // Función para obtener el endpoint del vendedor
    function getVendor() {
      return _getEndPoint() + OBJECTS.vendor + '/';
    }
    // Función para obtener los headers de seguridad de los servicios web
    function getGlobalHeaders() {
      return {
        type: 'vendor',
        Authorization: 'Basic ' + btoa(SessionValue.email + ':' + SessionValue.password),
        token: SessionValue.token,
        _id: btoa(SessionValue.id),
      };
    }

    /**
     * This function is only for testing a user which session expired
     */
    function getExpiredHeaders() {
      return {
        type: 'vendor',
        Authorization: 'Basic ' + btoa(SessionValue.email + ':' + SessionValue.password),
        expired: true, // Only for testing expired session
        token: SessionValue.token,
        _id: btoa(SessionValue.id),
      };
    }

    // Guarda el session token para despues usarlo en los headers
    function setSessionToken(token) {
      SessionValue.token = token;
    }

    // Guarda el id para despues usarlo en los headers
    function getSessionId() {
      return SessionValue.id;
    }

    // Guarda el usuario y el password para usarlo dentro de los headers
    function setUser(email, password) {
      SessionValue.email = email;
      SessionValue.password = password;
    }

    // Guarda el session id para usarlo dentro de los headers
    function setSessionId(id) {
      SessionValue.id = id;
    }
  }]);

}(angular.module("vrumProject", [
  // dependencias de la aplicación
  'sbAdminApp',
  'vrumProject.home',
  'vrumProject.about',
  'templates-app',
  'templates-common',
  'ui.router.state',
  'ui.router',
  'ui.bootstrap',
  'vrumProject.login',
  'vrumProject.signup',
  'vrumProject.dashboard',
  'common.ngFileSelect',
  'common.fileReader',
  'common.fileModel',
  'vrumProject.campaign',
  'vrumProject.CampaignTimeline',
])));
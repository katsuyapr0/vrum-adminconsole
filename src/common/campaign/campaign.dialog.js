(function(module) {

	// COntrolador del dialogo de campaña
	module.controller('CampaignDialogController', ['$scope', CampaignDialogController]);
	// Factory del campaign Dialog
	module.factory('CampaignDialog', CampaignDialogFactory);

	// Clase para crear un nuevo dialogo
	function AddCampaignDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show() {
			dialog = $modal.open({
				templateUrl: 'campaign/campaign.dialog.tpl.html',
				controller: 'CampaignDialogController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo AddCampaignDialog
	function CampaignDialogFactory($modal) {
		return new AddCampaignDialog($modal);
	}

	function CampaignDialogController($scope) {
		var model = this;

		init();

		// FUncion para obtener si es meridiano
		$scope.toggleMode = function() {
			$scope.ismeridian = !$scope.ismeridian;
		};

		// Funcion para actualizar el tiempo
		$scope.update = function() {
			var d = new Date();
			d.setHours(14);
			d.setMinutes(0);
			$scope.mytime = d;
		};

		// Funcion cuando cambia el tiempo
		$scope.changed = function() {
			$log.log('Time changed to: ' + $scope.mytime);
		};

		// funcion para limpiar el tiempo
		$scope.clear = function() {
			$scope.mytime = null;
		};

		// funcion para desactivar el tiempo
		$scope.disabled = function(date, mode) {
			return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
		};

		// Funcion para abrir el datetime-picker
		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		function init() {
			$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];

			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

			$scope.hstep = 1;
			$scope.mstep = 15;
			$scope.mytime = new Date();

			$scope.options = {
				hstep: [1, 2, 3],
				mstep: [1, 5, 10, 15, 25, 30]
			};

			$scope.ismeridian = true;
		}
	}

}(angular.module("vrumProject.campaign")));
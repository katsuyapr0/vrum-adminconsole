(function(module) {
  module.config(function ($stateProvider) {
    // Configura la ruta
    $stateProvider.state('campaign', {
      url: '/campaign',
      views: {
        "main": {
          controller: 'CampaignController as model',
          templateUrl: 'campaign/campaign.tpl.html'
        }
      },
      data:{ pageTitle: 'Campaign' }
    });
  });

}(angular.module("vrumProject.campaign", [
    'ui.router'
])));

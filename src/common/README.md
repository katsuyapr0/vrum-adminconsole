# The `src/common/` Directory

The `src/common/` directory houses internal and third-party re-usable
components. Essentially, this folder is for everything that isn't completely
specific to this application.

Each component resides in its own directory that may then be structured any way
the developer desires. The build system will read all `*.js` files that do not
end in `.spec.js` as source files to be included in the final build, all
`*.spec.js` files as unit tests to be executed, and all `*.tpl.html` files as
templates to compiled into the `$templateCache`. There is currently no way to
handle components that do not meet this pattern.

```
src/
  |- common/
  |  |- someComponent/
```

Every component contained here should be drag-and-drop reusable in any other 
project; they should depend on no other components that aren't similarly 
drag-and-drop reusable.

--------------------------------------------------------------------------------------------------

Estructura de carpetas

src/app: carpeta de modulos los cuales tienen rutas(e.g dashboard, home, login, etc.).
src/common: carpeta de modulos los cuales son generales(e.g campaign, campagin-timeline, etc.).
src/fonts: fuentes de la pagina web.

--------------------------------------------------------------------------------------------------

Estructura de modulos

Todo los modulos estan hechos bajo el paradigma de programación MVC

module-name.js: El controlador de la vista.
module-name.less: Estilos de los elementos que estan dentro del modulo.
module-name.model.js: Se declara un servicio que juega el papel de ser el modelo del controlador.
module-name.spec.js: Se supone que es para hacer TDT, pero no se usa, solo se crea por el generador que se tiene.
module-name.tpl.html: El html de todo el modulo.
module-name.web-services.js: Servicio con todos los servicios web que se usan dentro del modulo.

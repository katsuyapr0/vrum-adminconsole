(function(module) {
  module.directive('timeline', function() {
    return {
      templateUrl: 'sbAdminAngular/timeline.tpl.html',
      restrict: 'E',
      replace: true,
    };
  });

  module.directive('sidebar', ['$location', function() {
    return {
      templateUrl: 'sbAdminAngular/sidebar.tpl.html',
      restrict: 'E',
      replace: true,
      scope: {},
      controller: function($scope) {
        $scope.selectedMenu = 'dashboard';
        $scope.collapseVar = 0;
        $scope.multiCollapseVar = 0;

        $scope.check = function(x) {

          if (x == $scope.collapseVar) {
            $scope.collapseVar = 0;
          } else {
            $scope.collapseVar = x;
          }
        };

        $scope.multiCheck = function(y) {

          if (y == $scope.multiCollapseVar) {
            $scope.multiCollapseVar = 0;
          } else {
            $scope.multiCollapseVar = y;
          }
        };
      }
    };
  }]);

  module.directive('sidebarSearch', function() {
    return {
      templateUrl: 'sbAdminAngular/sidebar-search.tpl.html',
      restrict: 'E',
      replace: true,
      scope: {},
      controller: function($scope) {
        $scope.selectedMenu = 'home';
      }
    };
  });

  module.directive('notifications', function() {
    return {
      templateUrl: 'sbAdminAngular/notifications.tpl.html',
      restrict: 'E',
      replace: true,
    };
  });

  module.directive('header', function() {
    return {
      templateUrl: 'sbAdminAngular/header.tpl.html',
      restrict: 'E',
      replace: true,
    };
  });

  module.directive('headerNotification', function() {
    return {
      templateUrl: 'sbAdminAngular/header-notification.tpl.html',
      restrict: 'E',
      replace: true,
    };
  });

  module.directive('stats', function() {
    return {
      templateUrl: 'sbAdminAngular/stats.tpl.html',
      restrict: 'E',
      replace: true,
      scope: {
        'model': '=',
        'comments': '@',
        'number': '@',
        'name': '@',
        'colour': '@',
        'details': '@',
        'type': '@'
      }
    };
  });

  module.directive('chat', function() {
    return {
      templateUrl: 'sbAdminAngular/chat.tpl.html',
      restrict: 'E',
      replace: true,
    };
  });

  module.controller('ChartCtrl', ['$scope', '$timeout', function($scope, $timeout) {
    console.log('I enter ChartCtrl!');

    $scope.ome = 'ome huevon ome!';

    $scope.line = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      series: ['Series A', 'Series B'],
      data: [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
      ],
      onClick: function(points, evt) {
        console.log(points, evt);
      }
    };

    $scope.bar = {
      labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
      series: ['Series A', 'Series B'],

      data: [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
      ]

    };

    $scope.donut = {
      labels: ["Download Sales", "In-Store Sales", "Mail-Order Sales"],
      data: [300, 500, 100]
    };

    $scope.radar = {
      labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],

      data: [
        [65, 59, 90, 81, 56, 55, 40],
        [28, 48, 40, 19, 96, 27, 100]
      ]
    };

    $scope.pie = {
      labels: ["Download Sales", "In-Store Sales", "Mail-Order Sales"],
      data: [300, 500, 100]
    };

    $scope.polar = {
      labels: ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"],
      data: [300, 500, 100, 40, 120]
    };

    $scope.dynamic = {
      labels: ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"],
      data: [300, 500, 100, 40, 120],
      type: 'PolarArea',

      toggle: function() {
        this.type = this.type === 'PolarArea' ?
          'Pie' : 'PolarArea';
      }
    };
  }]);
})(angular.module('sbAdminApp', [
  'ui.bootstrap',
]));
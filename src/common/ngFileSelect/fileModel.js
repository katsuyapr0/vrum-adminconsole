(function(app) {
	app.directive('fileModel', ['$parse', function($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	}]);

	app.service('fileUpload', ['$http', function($http) {
		this.uploadFileToUrl = function(file, uploadUrl, type, success, error) {
			var fd = new FormData();
			fd.append('file', file);
			fd.append('type', type);
			$http.put(uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined
					}
				})
				.success(success)
				.error(error);
		};
	}]);
})(angular.module("common.fileModel", []));
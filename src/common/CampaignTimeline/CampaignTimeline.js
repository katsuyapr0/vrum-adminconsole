(function(module) {
	// COntrolador del timeline
	module.controller('CampaignTimelineController', CampaignTimelineController);
	// directiva del timeline
	module.directive('campaignTimeline', CampaignTimelineDirective);

	function CampaignTimelineController() {
		var model = this;

		init();

		function isInverseSide(index) {
			return index%2 !== 0;
		}

		function init() {

		}

		model.isInverseSide = isInverseSide;
	}

	function CampaignTimelineDirective() {
		return {
			restrict: 'E',
			controller: 'CampaignTimelineController as model',
			templateUrl: 'campaignTimeline/campaignTimeline.tpl.html',
			scope: {
				campaigns: '=',
			}
		};
	}

}(angular.module("vrumProject.CampaignTimeline")));
(function(module) {
  module.config(function ($stateProvider) {
    // COnfigura la ruta del campaigntimeline
    $stateProvider.state('CampaignTimeline', {
      url: '/campaigntimeline',
      views: {
        "main": {
          controller: 'CampaignTimelineController as model',
          templateUrl: 'campaignTimeline/campaignTimeline.tpl.html'
        }
      },
      data:{ pageTitle: 'CampaignTimeline' }
    });
  });

}(angular.module("vrumProject.CampaignTimeline", [
    'ui.router'
])));
